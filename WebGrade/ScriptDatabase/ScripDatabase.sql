create database webgrade;

use webgrade;

create table turma ( cod_turma varchar(20), modalidade varchar(20), ano varchar(20), curso varchar(20),primary key (cod_turma)) engine=innodb;

create table professor ( nome varchar(50), ra_professor int, disciplina varchar(20), primary key (ra_professor)) engine=innodb;

create table disciplina (nome varchar(50), cod_disciplina varchar(20), professor varchar(20), primary key(cod_disciplina)) engine=innodb;

create table prova (nota float, cod_prova int, data date, conteudo varchar(100), ra_professor int, cod_disciplina varchar(20), primary key(cod_prova)) engine=innodb;

create table aluno ( nome varchar(50), ra_aluno int,serie varchar(20), cod_turma varchar(20), cod_prova int, primary key (ra_aluno)) engine=innodb;

create table admin(nome varchar(50), primary key (nome)) engine=innodb;

alter table aluno add foreign key (cod_turma) references turma(cod_turma),add foreign key (cod_prova) references prova(cod_prova);

alter table prova add foreign key (ra_professor) references professor(ra_professor), add foreign key (cod_disciplina) references disciplina(cod_disciplina);

alter table disciplina add cod_turma varchar(20);
alter table disciplina add foreign key (cod_turma) references turma(cod_turma);

alter table turma add cod_disciplina varchar(20);
alter table turma add foreign key (cod_disciplina) references disciplina(cod_disciplina);

create table alunoProva( ra_aluno int, cod_prova int)  engine=innodb;
create table turmaProfessor( ra_professor int, cod_turma varchar(20))  engine=innodb;

alter table alunoProva add foreign key (ra_aluno) references aluno(ra_aluno), add foreign key (cod_prova) references prova(cod_prova);
alter table turmaProfessor add foreign key (ra_professor) references professor(ra_professor), add foreign key (cod_turma) references turma(cod_turma);

create table loginProfessor(ra int, pass varchar(44), primary key (ra), foreign key (ra) references professor(ra_professor)) engine=innodb;
create table loginAluno(ra int, pass varchar(44), primary key (ra), foreign key (ra) references aluno(ra_aluno)) engine=innodb;
create table loginAdmin(User varchar(50), pass varchar(44), primary key (User), foreign key (User) references Admin(nome)) engine=innodb;