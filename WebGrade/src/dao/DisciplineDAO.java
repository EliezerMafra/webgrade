package dao;
//TESTE PC NUBIA
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.MySql;
import model.Discipline;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import controler.ConnectionFactory;

public class DisciplineDAO {

private MySql model = null;
	
	public DisciplineDAO(){
		super();
		
		model = new MySql();
		
		model.setDatabase("WebGrade");
		model.setServer("localhost");
		model.setPort("3306");
		model.setUser("root");
		model.setPassword("root");
	}

	public boolean Create(Discipline Discipline){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "INSERT INTO disciplina VALUES (?,?,?);";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		
			stmt.setString(1, Discipline.getName());
			stmt.setString(2, Discipline.getCod());
			stmt.setString(3, Discipline.getTeacher());
			
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Update(Discipline Discipline, String key){
		
		boolean status = true;
		
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "UPDATE disciplina SET nome= ?, cod_disciplina= ?, professor= ? WHERE cod_disciplina = ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		
			stmt.setString(1, Discipline.getName());
			stmt.setString(2, Discipline.getCod());
			stmt.setString(3, Discipline.getTeacher());
			stmt.setString(4, key);
			
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Delete(String key){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!");
			return false;
		}
		
		String sql = "DELETE FROM disciplina WHERE cod_disciplina = ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			
			stmt.setString(1, key);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public Discipline Read(String key){
		
		Discipline Discipline = new Discipline();
		 
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if( con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR!!!!!");
		}

		String sql = "SELECT * FROM disciplina WHERE cod_disciplina=?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			stmt.setString(1, key);
			rs = stmt.executeQuery();
			
			rs.next();
			Discipline.setName(rs.getString("nome"));
			Discipline.setCod(rs.getString("cod_disciplina"));
			Discipline.setTeacher(rs.getString("professor"));
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		return Discipline;
		
	}
	//LIST------------------------------------------------------------------------------------------
	public ArrayList<Discipline> List(){
		
		ArrayList<Discipline> DisciplineArray = new ArrayList<Discipline>();
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR");
		}
		
		String sql = "SELECT * FROM disciplina;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			rs = stmt.executeQuery();
			
			while(rs.next()){
				Discipline Discipline = new Discipline();
				Discipline.setName(rs.getString("nome"));
				Discipline.setCod(rs.getString("cod_disciplina"));
				Discipline.setTeacher(rs.getString("professor"));
				DisciplineArray.add(Discipline);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return DisciplineArray;
		
	}
	
}
