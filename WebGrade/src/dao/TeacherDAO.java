package dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import controler.ConnectionFactory;
import model.MySql;
import model.Teacher;

public class TeacherDAO {
	
	private MySql model = null;
	
	public TeacherDAO(){
		super();
		
		model = new MySql();
		
		model.setDatabase("WebGrade");
		model.setServer("localhost");
		model.setPort("3306");
		model.setUser("root");
		model.setPassword("root");
	}

	public boolean Create(Teacher teacher){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "INSERT INTO Professor VALUES (?,?,?);";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		
			stmt.setString(1, teacher.getName());
			stmt.setInt(2, teacher.getRa());
			stmt.setString(3, teacher.getDiscipline());
			
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Update(Teacher teacher, int key){
		
		boolean status = true;
		
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "UPDATE Professor SET nome= ?, ra_professor = ?, disciplina= ? WHERE ra_professor = ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		
			stmt.setString(1, teacher.getName());
			stmt.setInt(2, teacher.getRa());
			stmt.setString(3, teacher.getDiscipline());
			stmt.setInt(4, key);
			
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Delete(int key){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!");
			return false;
		}
		
		String sql = "DELETE FROM professor WHERE ra_professor = ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			
			stmt.setInt(1, key);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public Teacher Read(int key){
		
		Teacher teacher = new Teacher();
		 
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if( con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR!!!!!");
		}

		String sql = "SELECT * FROM professor WHERE ra_professor=?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			stmt.setInt(1, key);
			rs = stmt.executeQuery();
			
			rs.next();
			teacher.setName(rs.getString("nome"));
			teacher.setRa(Integer.valueOf(rs.getString("ra_professor")));
			teacher.setDiscipline(rs.getString("disciplina"));
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		return teacher;
		
	}
	//LIST------------------------------------------------------------------------------------------
	public ArrayList<Teacher> List(){
		
		ArrayList<Teacher> teacherArray = new ArrayList<Teacher>();
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR");
		}
		
		String sql = "SELECT * FROM professor;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			rs = stmt.executeQuery();
			
			while(rs.next()){
				Teacher teacher = new Teacher();
				teacher.setName(rs.getString("nome"));
				teacher.setRa(Integer.valueOf(rs.getString("ra_professor")));
				teacher.setDiscipline(rs.getString("disciplina"));
				teacherArray.add(teacher);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return teacherArray;
		
	}
	
}
