package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import controler.ConnectionFactory;
import model.Class;
import model.MySql;
import model.Student;

public class LoginDAO {
	
	private MySql model = null;
	
	public LoginDAO(){
		super();
		
		model = new MySql();
		
		model.setDatabase("WebGrade");
		model.setServer("localhost");
		model.setPort("3306");
		model.setUser("root");
		model.setPassword("root");
	}

	public boolean Validation(int ra, String password){
		
		boolean status = false;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "select * from login where ra=? and password=?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
		
			stmt.setInt(1, ra);
			stmt.setString(2, password);
			
			stmt.executeQuery();
			
			if(rs.next())
				status = true;
			else
				status = false;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean ValidationAdmin(String user, String password){
		
		boolean status = false;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "select * from loginAdmin where user=? and pass=?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
		
			stmt.setString(1, user);
			stmt.setString(2, password);
			
			stmt.executeQuery();
			
			if(rs.next())
				status = true;
			else
				status = false;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	

	

	

	
}
