package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import controler.ConnectionFactory;
import model.Class;
import model.MySql;
import model.Student;

public class ClassDAO {
	
	private MySql model = null;
	
	public ClassDAO(){
		super();
		
		model = new MySql();
		
		model.setDatabase("WebGrade");
		model.setServer("localhost");
		model.setPort("3306");
		model.setUser("root");
		model.setPassword("root");
	}

	public boolean Create(Class Cclass){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "INSERT INTO turma VALUES (?,?,?);";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		
			stmt.setString(1, Cclass.getCod());
			stmt.setString(2, Cclass.getModalidade());
			stmt.setString(3, Cclass.getAno());
			stmt.setString(4, Cclass.getCurso());
			
					
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Update(Class Cclass, int key){
		
		boolean status = true;
		
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "UPDATE turma SET cod_turma= ?, modalidade= ?, ano=?, curso=?,  WHERE cod_turma= ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		

			stmt.setString(1, Cclass.getCod());
			stmt.setString(2, Cclass.getModalidade());
			stmt.setString(3, Cclass.getAno());
			stmt.setString(4, Cclass.getCurso());
			stmt.setInt(5,key);
			
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Delete(int key){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!");
			return false;
		}
		
		String sql = "DELETE FROM turma WHERE cod_turma = ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			
			stmt.setInt(1, key);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public Class Read(int key){
		
		Class Cclass = new Class();
		 
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if( con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR!!!!!");
		}

		String sql = "SELECT * FROM turma WHERE cod_turma=?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			stmt.setInt(1, key);
			rs = stmt.executeQuery();
			
			rs.next();
			Cclass.setCod(rs.getString("cod_turma"));
			Cclass.setModalidade(rs.getString("modalidade"));
			Cclass.setAno(rs.getString("ano"));
			Cclass.setCurso(rs.getString("curso"));
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		return Cclass;
		
	}
	//LIST------------------------------------------------------------------------------------------
	public ArrayList<Class> List(){
		
		ArrayList<Class> CclassArray = new ArrayList<Class>();
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR");
		}
		
		String sql = "SELECT * FROM turma;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			rs = stmt.executeQuery();
			
			while(rs.next()){
				Class Cclass = new Class();
				Cclass.setCod(rs.getString("cod_turma"));
				Cclass.setModalidade(rs.getString("modalidade"));
				Cclass.setAno(rs.getString("ano"));
				Cclass.setCurso(rs.getString("curso"));
				CclassArray.add(Cclass);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return CclassArray;
		
	}
	
}
