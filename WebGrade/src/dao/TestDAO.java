package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import controler.ConnectionFactory;
import model.Test;
import model.TestImport;
import model.MySql;

public class TestDAO {
	
private MySql model = null;
	
	public TestDAO(){
		super();
		
		model = new MySql();
		
		model.setDatabase("WebGrade");
		model.setServer("localhost");
		model.setPort("3306");
		model.setUser("root");
		model.setPassword("root");
	}

	public boolean Create(Test Test, int raTeacher, int codDisciplina, double grade){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "INSERT INTO prova VALUES (?,?,?,?,?,?);";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		
			stmt.setDouble(1, grade);
			stmt.setInt(2, Test.getCod());
			stmt.setString(3, Test.getData());
			stmt.setString(4, Test.getContents());
			stmt.setInt(5,raTeacher );
			stmt.setInt(6, codDisciplina);
			
					
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Update(Test Test, int raTeacher, int codDisciplina, double grade, int key){
		
		boolean status = true;
		
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "UPDATE prova SET nota=?, cod_prova=?, data=?, conteudo=?, ra_professor=?, cod_disciplina=?  WHERE cod= ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		

			stmt.setDouble(1, grade);
			stmt.setInt(2, Test.getCod());
			stmt.setString(3, Test.getData());
			stmt.setString(4, Test.getContents());
			stmt.setInt(5,raTeacher );
			stmt.setInt(6, codDisciplina);
			stmt.setInt(7,key);
			
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Delete(int key){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!");
			return false;
		}
		
		String sql = "DELETE FROM Test WHERE cod = ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			
			stmt.setInt(1, key);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public TestImport Read(int key){
		
		TestImport Test = new TestImport();
		 
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if( con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR!!!!!");
		}

		String sql = "SELECT * FROM student WHERE cod=?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			stmt.setInt(1, key);
			rs = stmt.executeQuery();
			
			Test normalTest = new Test();
			normalTest = Test.getTest();
			
			rs.next();
			normalTest.setCod(rs.getInt("cod"));
			normalTest.setContents(rs.getString("conteudo"));
			normalTest.setData(String.valueOf(rs.getDate("data")));
			Test.setGrade(rs.getDouble("nota"));
			Test.setRaTeacher(rs.getInt("ra_professor"));
			Test.setCodDisciplina(rs.getInt("cod_disciplina"));
			
			
			Test.setTest(normalTest);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		return Test;
		
	}
	//LIST------------------------------------------------------------------------------------------
	public ArrayList<TestImport> List(){
		
		ArrayList<TestImport> TestArray = new ArrayList<TestImport>();
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR");
		}
		
		String sql = "SELECT * FROM student;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			rs = stmt.executeQuery();
			
			while(rs.next()){
				TestImport normalTest = new TestImport();
				Test test = normalTest.getTest();
				
				rs.next();
				test.setCod(rs.getInt("cod"));
				test.setContents(rs.getString("conteudo"));
				test.setData(String.valueOf(rs.getDate("data")));
				normalTest.setGrade(rs.getDouble("nota"));
				normalTest.setRaTeacher(rs.getInt("ra_professor"));
				normalTest.setCodDisciplina(rs.getInt("cod_disciplina"));
				
				
				normalTest.setTest(test);
				
				TestArray.add(normalTest);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return TestArray;
		
	}

}
