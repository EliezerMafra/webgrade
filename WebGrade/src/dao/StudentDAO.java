package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import controler.ConnectionFactory;
import model.MySql;
import model.Student;

public class StudentDAO {
	
	private MySql model = null;
	
	public StudentDAO(){
		super();
		
		model = new MySql();
		
		model.setDatabase("WebGrade");
		model.setServer("localhost");
		model.setPort("3306");
		model.setUser("root");
		model.setPassword("root");
	}

	public boolean Create(Student student){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "INSERT INTO aluno VALUES (?,?,?);";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		
			stmt.setString(1, student.getName());
			stmt.setInt(2, student.getRa());
			stmt.setString(3, student.getSerie());
			
			System.out.println(stmt);
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Update(Student student, int key){
		
		boolean status = true;
		
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!!");
			return false;
		}
		
		String sql = "UPDATE aluno SET nome= ?, ra_aluno= ?, serie= ? WHERE ra_aluno= ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
		
			stmt.setString(1, student.getName());
			stmt.setInt(2, student.getRa());
			stmt.setString(3, student.getSerie());
			stmt.setInt(4, key);
			
			stmt.executeUpdate();
			status = true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public boolean Delete(int key){
		
		boolean status = true;
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if(con==null){
			System.out.println("DATABASE CONNECTION ERROR!!!!");
			return false;
		}
		
		String sql = "DELETE FROM aluno WHERE ra_aluno = ?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			
			stmt.setInt(1, key);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		
		return status;
	}
	
	public Student Read(int key){
		
		Student student = new Student();
		 
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if( con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR!!!!!");
		}

		String sql = "SELECT * FROM aluno WHERE ra_aluno=?;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			stmt.setInt(1, key);
			rs = stmt.executeQuery();
			
			rs.next();
			student.setName(rs.getString("nome"));
			student.setRa(Integer.valueOf(rs.getString("ra_aluno")));
			student.setSerie(rs.getString("serie"));
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con);
		}
		return student;
		
	}
	//LIST------------------------------------------------------------------------------------------
	public ArrayList<Student> List(){
		
		ArrayList<Student> studentArray = new ArrayList<Student>();
		Connection con = (Connection) ConnectionFactory.getConnection(model);
		
		if (con == null){
			JOptionPane.showMessageDialog(null, "DATABASE CONNECTION ERROR");
		}
		
		String sql = "SELECT * FROM aluno;";
		
		try {
			PreparedStatement stmt = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = null;
			
			rs = stmt.executeQuery();
			
			while(rs.next()){
				Student student = new Student();
				student.setName(rs.getString("nome"));
				student.setRa(Integer.valueOf(rs.getString("ra_aluno")));
				student.setSerie(rs.getString("serie"));
				studentArray.add(student);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con);
		}
		
		return studentArray;
		
	}
	
}
