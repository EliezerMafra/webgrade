package view.LoginGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JPasswordField;

public class LoginAluno extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginAluno frame = new LoginAluno();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginAluno() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 440, 285);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(156, 125, 134, 20);
		contentPane.add(passwordField);
		
		JLabel lblLoginAluno = new JLabel("Login Aluno");
		lblLoginAluno.setForeground(Color.WHITE);
		lblLoginAluno.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 34));
		lblLoginAluno.setBounds(130, 11, 174, 43);
		contentPane.add(lblLoginAluno);
		
		JLabel label = new JLabel("Ra:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setForeground(Color.WHITE);
		label.setBounds(95, 90, 52, 23);
		contentPane.add(label);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(155, 91, 133, 21);
		contentPane.add(textField);
		
		JLabel label_1 = new JLabel("Senha:");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(95, 124, 52, 23);
		contentPane.add(label_1);
		
		JButton button = new JButton("Entrar");
		button.setBounds(262, 180, 89, 23);
		contentPane.add(button);
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(LoginAluno.class.getResource("/view/img/Estudante4.jpg")));
		label_2.setBounds(0, -148, 922, 529);
		contentPane.add(label_2);
	}

}
