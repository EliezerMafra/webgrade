package view.LoginGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JPasswordField;

public class LoginProfessor extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginProfessor frame = new LoginProfessor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginProfessor() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 420, 264);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(170, 113, 116, 20);
		contentPane.add(passwordField);
		
		JLabel lblLoginProfessor = new JLabel("Login Professor");
		lblLoginProfessor.setForeground(Color.WHITE);
		lblLoginProfessor.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 34));
		lblLoginProfessor.setBounds(91, 13, 226, 43);
		contentPane.add(lblLoginProfessor);
		
		JLabel lblRa = new JLabel("Ra:");
		lblRa.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRa.setForeground(Color.WHITE);
		lblRa.setBounds(108, 78, 52, 23);
		contentPane.add(lblRa);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSenha.setForeground(Color.WHITE);
		lblSenha.setBounds(108, 111, 52, 23);
		contentPane.add(lblSenha);
		
		textField = new JTextField();
		textField.setBounds(170, 79, 117, 21);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(269, 177, 89, 23);
		contentPane.add(btnEntrar);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(LoginProfessor.class.getResource("/view/img/school-classroom-chalkboard-desk.jpg")));
		lblNewLabel.setBounds(0, 0, 402, 253);
		contentPane.add(lblNewLabel);
	}
}
