package view.LoginGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import dao.LoginDAO;

public class LoginAdministrador extends JFrame {

	private JPanel contentPane;
	private JTextField txf_user;
	private JButton button;
	private JLabel label_1;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginAdministrador frame = new LoginAdministrador();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginAdministrador() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 401, 282);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(198,226,255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLoginAdministrador = new JLabel("Login Administrador");
		lblLoginAdministrador.setForeground(Color.WHITE);
		lblLoginAdministrador.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 34));
		lblLoginAdministrador.setBounds(50, 12, 316, 43);
		contentPane.add(lblLoginAdministrador);
		
		JLabel lblUser = new JLabel("User:");
		lblUser.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblUser.setForeground(Color.BLACK);
		lblUser.setBounds(120, 99, 52, 23);
		contentPane.add(lblUser);
		
		txf_user = new JTextField();
		txf_user.setColumns(10);
		txf_user.setBounds(165, 100, 113, 21);
		contentPane.add(txf_user);
		
		JLabel label = new JLabel("Senha:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setForeground(Color.BLACK);
		label.setBounds(119, 133, 52, 23);
		contentPane.add(label);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(166, 136, 111, 20);
		contentPane.add(passwordField);
		
		button = new JButton("Entrar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				LoginDAO dao = new LoginDAO();
				
				String user = txf_user.getText();
				String pass = passwordField.getText();
				
				
				dao.ValidationAdmin(user, pass);
				
			}
		});
		button.setBounds(276, 191, 89, 23);
		contentPane.add(button);
		
		label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(LoginAdministrador.class.getResource("/view/img/Admin1.jpg")));
		label_1.setBounds(-73, -54, 534, 307);
		contentPane.add(label_1);
	}
}
