package view.LoginGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class LoginGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI frame = new LoginGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 426, 267);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWebGrade = new JLabel("WEB GRADE");
		lblWebGrade.setForeground(new Color(0,154,205));
		lblWebGrade.setBackground(new Color(135, 206, 235));
		lblWebGrade.setFont(new Font("Calibri", Font.ITALIC, 41));
		lblWebGrade.setBounds(124, 14, 261, 51);
		contentPane.add(lblWebGrade);
		
		JComboBox cb = new JComboBox();
		cb.setForeground(new Color(0, 0, 0));
		cb.setBackground(new Color(248,248,255));
		cb.setModel(new DefaultComboBoxModel(new String[] {"Op\u00E7\u00F5es", "Professor", "Aluno", "Administrador"}));
		cb.setBounds(43, 108, 177, 26);
		contentPane.add(cb);
		
		JLabel lblLogin = new JLabel("");
		lblLogin.setBounds(31, 58, 66, 20);
		contentPane.add(lblLogin);
		
		JButton Entrar = new JButton("Entrar");
		Entrar.setBackground(new Color(198,226,255));
		Entrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			String web2 = "Professor";
			String web3 = "Aluno";
			String web4= "Administrador";
		
			
			String web = cb.getSelectedItem().toString();
			
			if(web.equals(web2)){
				
				LoginProfessor x = new LoginProfessor();
				x.setVisible(true);}
				
				if(web.equals(web3)){
					
					LoginAluno x1 = new LoginAluno();
					x1.setVisible(true);
				
			}
						if(web.equals(web4)){
						LoginAdministrador x2 = new LoginAdministrador();
						x2.setVisible(true);
			
			} }
				
	
					
		});
		Entrar.setBounds(251, 106, 89, 30);
		contentPane.add(Entrar);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(LoginGUI.class.getResource("/view/img/w.jpg")));
		label.setBounds(324, 160, 154, 64);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(LoginGUI.class.getResource("/view/img/Sococ.png")));
		label_1.setBounds(-1, -46, 718, 361);
		contentPane.add(label_1);
	}
}
