package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Student;
import controler.StudentCTRL;
import dao.StudentDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.Color;
import javax.swing.ImageIcon;

public class StudentGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txf_Nome_Cadastro;
	private JTextField txf_Ra_Cadastro;
	private JTextField txf_Serie_Cadastro;
	private JTextField txf_Nome_Editar;
	private JTextField txf_Ra_Editar;
	private JTextField txf_Serie_Editar;
	private JTextField txf_Key_Editar;
	private JTextField txf_Ra_Deletar;
	private JTextField txf_Nome_Pesquisa;
	private JTextField txf_Ra_Pesquisa;
	private JTextField txf_Serie_Pesquisa;
	private JTextField txf_Key_Pesquisa;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentGUI frame = new StudentGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 312);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240,255,240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAluno = new JLabel("Aluno");
		lblAluno.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblAluno.setBounds(158, 13, 106, 37);
		contentPane.add(lblAluno);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 61, 408, 201);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Cadastro", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(12, 13, 56, 16);
		panel.add(lblNome);
		
		txf_Nome_Cadastro = new JTextField();
		txf_Nome_Cadastro.setBounds(80, 10, 311, 22);
		panel.add(txf_Nome_Cadastro);
		txf_Nome_Cadastro.setColumns(10);
		
		JLabel lblRa = new JLabel("RA:");
		lblRa.setBounds(12, 42, 56, 16);
		panel.add(lblRa);
		
		txf_Ra_Cadastro = new JTextField();
		txf_Ra_Cadastro.setBounds(80, 39, 311, 22);
		panel.add(txf_Ra_Cadastro);
		txf_Ra_Cadastro.setColumns(10);
		
		JLabel lblSrie = new JLabel("S\u00E9rie:");
		lblSrie.setBounds(12, 71, 56, 16);
		panel.add(lblSrie);
		
		txf_Serie_Cadastro = new JTextField();
		txf_Serie_Cadastro.setBounds(80, 68, 311, 22);
		panel.add(txf_Serie_Cadastro);
		txf_Serie_Cadastro.setColumns(10);
		
		JButton btn_Cadastro = new JButton("Cadastro");
		btn_Cadastro.setBackground(new Color(245,255,250));
		btn_Cadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String nome = txf_Nome_Cadastro.getText();
				int ra = Integer.parseInt(txf_Ra_Cadastro.getText());
				String serie = txf_Serie_Cadastro.getText();
				
				StudentCTRL ctrl = new StudentCTRL();
				Student student = new Student(nome,ra,serie);
				
				ctrl.Create(student);	
			}
		});
		btn_Cadastro.setBounds(294, 103, 97, 25);
		panel.add(btn_Cadastro);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Editar", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel label = new JLabel("Nome:");
		label.setBounds(12, 43, 56, 16);
		panel_1.add(label);
		
		txf_Nome_Editar = new JTextField();
		txf_Nome_Editar.setColumns(10);
		txf_Nome_Editar.setBounds(80, 40, 311, 22);
		panel_1.add(txf_Nome_Editar);
		
		JLabel label_1 = new JLabel("RA:");
		label_1.setBounds(12, 72, 56, 16);
		panel_1.add(label_1);
		
		txf_Ra_Editar = new JTextField();
		txf_Ra_Editar.setColumns(10);
		txf_Ra_Editar.setBounds(80, 69, 311, 22);
		panel_1.add(txf_Ra_Editar);
		
		JLabel label_2 = new JLabel("S\u00E9rie:");
		label_2.setBounds(12, 101, 56, 16);
		panel_1.add(label_2);
		
		txf_Serie_Editar = new JTextField();
		txf_Serie_Editar.setColumns(10);
		txf_Serie_Editar.setBounds(80, 98, 311, 22);
		panel_1.add(txf_Serie_Editar);
		
		JButton btn_Editar = new JButton("Editar");
		btn_Editar.setBackground(new Color(245,255,250));
		btn_Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String nome = txf_Nome_Editar.getText();
				int ra = Integer.parseInt (txf_Ra_Editar.getText());
				String serie = txf_Serie_Editar.getText();
				int key = Integer.parseInt(txf_Key_Editar.getText());
				
				Student student = new Student(nome,ra,serie);
				
				StudentCTRL ctrl = new StudentCTRL();
				ctrl.Update(student, key);
				
			}
		});
		btn_Editar.setBounds(294, 133, 97, 25);
		panel_1.add(btn_Editar);
		
		JLabel lblRa_1 = new JLabel("RA:");
		lblRa_1.setBounds(12, 13, 56, 16);
		panel_1.add(lblRa_1);
		
		txf_Key_Editar = new JTextField();
		txf_Key_Editar.setBounds(80, 10, 116, 22);
		panel_1.add(txf_Key_Editar);
		txf_Key_Editar.setColumns(10);
		
		JButton btn_Pesquisar_Editar = new JButton("Pesquisar");
		btn_Pesquisar_Editar.setBackground(new Color(245,255,250));
		btn_Pesquisar_Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				StudentDAO dao = new StudentDAO();
				
				StudentCTRL ctrl = new StudentCTRL();
				
				int key = Integer.valueOf(txf_Key_Editar.getText());
				
				Student student = ctrl.Read(key);
				
				txf_Nome_Editar.setText(student.getName());
				txf_Ra_Editar.setText(String.valueOf(student.getRa()));
				txf_Serie_Editar.setText(student.getSerie());
				
			}
		});
		btn_Pesquisar_Editar.setBounds(208, 9, 97, 25);
		panel_1.add(btn_Pesquisar_Editar);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Deletar", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblRa_2 = new JLabel("RA:");
		lblRa_2.setBounds(12, 13, 56, 16);
		panel_2.add(lblRa_2);
		
		txf_Ra_Deletar = new JTextField();
		txf_Ra_Deletar.setBounds(80, 10, 116, 22);
		panel_2.add(txf_Ra_Deletar);
		txf_Ra_Deletar.setColumns(10);
		
		JButton btn_Deletar = new JButton("Deletar");
		btn_Deletar.setBackground(new Color(245,255,250));
		btn_Deletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
                int ra = Integer.parseInt(txf_Ra_Deletar.getText());
				
                StudentCTRL ctrl = new StudentCTRL();
				
				ctrl.Delete(ra);
				
			}
		});
		btn_Deletar.setBounds(208, 9, 97, 25);
		panel_2.add(btn_Deletar);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Pesquisa", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel label_3 = new JLabel("Nome:");
		label_3.setBounds(12, 61, 56, 16);
		panel_3.add(label_3);
		
		txf_Nome_Pesquisa = new JTextField();
		txf_Nome_Pesquisa.setBackground(new Color(245,245,245));
		txf_Nome_Pesquisa.setEditable(false);
		txf_Nome_Pesquisa.setColumns(10);
		txf_Nome_Pesquisa.setBounds(80, 58, 311, 22);
		panel_3.add(txf_Nome_Pesquisa);
		
		JLabel label_4 = new JLabel("RA:");
		label_4.setBounds(12, 90, 56, 16);
		panel_3.add(label_4);
		
		txf_Ra_Pesquisa = new JTextField();
		txf_Ra_Pesquisa.setBackground(new Color(245,245,245));
		txf_Ra_Pesquisa.setEditable(false);
		txf_Ra_Pesquisa.setColumns(10);
		txf_Ra_Pesquisa.setBounds(80, 87, 311, 22);
		panel_3.add(txf_Ra_Pesquisa);
		
		JLabel label_5 = new JLabel("S\u00E9rie:");
		label_5.setBounds(12, 119, 56, 16);
		panel_3.add(label_5);
		
		txf_Serie_Pesquisa = new JTextField();
		txf_Serie_Pesquisa.setBackground(new Color(245,245,245));
		txf_Serie_Pesquisa.setEditable(false);
		txf_Serie_Pesquisa.setColumns(10);
		txf_Serie_Pesquisa.setBounds(80, 116, 311, 22);
		panel_3.add(txf_Serie_Pesquisa);
		
		JLabel label_6 = new JLabel("RA:");
		label_6.setBounds(12, 31, 56, 16);
		panel_3.add(label_6);
		
		txf_Key_Pesquisa = new JTextField();
		txf_Key_Pesquisa.setColumns(10);
		txf_Key_Pesquisa.setBounds(80, 28, 116, 22);
		panel_3.add(txf_Key_Pesquisa);
		
		JButton btn_Pesquisar_Pesquisa = new JButton("Pesquisar");
		btn_Pesquisar_Pesquisa.setBackground(new Color(245,255,250));
		btn_Pesquisar_Pesquisa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				StudentDAO dao = new StudentDAO();
				
				StudentCTRL ctrl = new StudentCTRL();
				
				int key = Integer.valueOf(txf_Key_Editar.getText());
				
				Student student = ctrl.Read(key);
				
				txf_Nome_Editar.setText(student.getName());
				txf_Ra_Editar.setText(String.valueOf(student.getRa()));
				txf_Serie_Editar.setText(student.getSerie());
				
			}
		});
		btn_Pesquisar_Pesquisa.setBounds(208, 27, 97, 25);
		panel_3.add(btn_Pesquisar_Pesquisa);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Listagem", null, panel_4, null);
		panel_4.setLayout(null);
		
		JButton btn_Listar = new JButton("Listar");
		btn_Listar.setBackground(new Color(245,255,250));
		btn_Listar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DefaultTableModel modelTable = (DefaultTableModel) table.getModel();
				
				ArrayList<String> row= new ArrayList<String>();
				
				modelTable.setNumRows(0);
				
				StudentDAO dao = new StudentDAO();
				
				ArrayList<Student> listStudent = dao.List();
				
				for (Student a : listStudent ){
					
					row.add(a.getName());
					row.add(String.valueOf(a.getRa()));
					row.add(a.getSerie());
					modelTable.addRow(row.toArray());
					row.clear();
				}
				
			}
		});
		btn_Listar.setBounds(294, 13, 97, 25);
		panel_4.add(btn_Listar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 55, 391, 103);
		panel_4.add(scrollPane);
		
		table = new JTable();
		table.setBackground(new Color(198,226,255));
		table.setFillsViewportHeight(true);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nome", "RA", "S\u00E9rie"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(StudentGUI.class.getResource("/view/img/Icone_certo_aluno2.png")));
		lblNewLabel.setBounds(262, 0, 172, 80);
		contentPane.add(lblNewLabel);
	}
}
