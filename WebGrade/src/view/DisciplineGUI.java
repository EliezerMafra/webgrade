package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Discipline;
import controler.DisciplineCTRL;
import dao.DisciplineDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.Color;
import javax.swing.ImageIcon;

public class DisciplineGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txf_Nome_Cadastro;
	private JTextField txf_Codigo_Cadastro;
	private JTextField txf_Teacher_Cadastro;
	private JTextField txf_Nome_Editar;
	private JTextField txf_Codigo_Editar;
	private JTextField txf_Teacher_Editar;
	private JTextField txf_Key_Editar;
	private JTextField txf_key_Deletar;
	private JTextField txf_Nome_Pesquisa;
	private JTextField txf_Codigo_Pesquisa;
	private JTextField txf_Teacher_Pesquisa;
	private JTextField txf_Key_Pesquisa;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DisciplineGUI frame = new DisciplineGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DisciplineGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 486, 320);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240,255,240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDisciplina = new JLabel("Disciplina");
		lblDisciplina.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblDisciplina.setBounds(132, 11, 188, 37);
		contentPane.add(lblDisciplina);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 72, 435, 201);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Cadastro", null, panel, null);
		panel.setLayout(null);
		
		txf_Nome_Cadastro = new JTextField();
		txf_Nome_Cadastro.setBounds(80, 10, 311, 22);
		panel.add(txf_Nome_Cadastro);
		txf_Nome_Cadastro.setColumns(10);
		
		txf_Codigo_Cadastro = new JTextField();
		txf_Codigo_Cadastro.setBounds(80, 39, 311, 22);
		panel.add(txf_Codigo_Cadastro);
		txf_Codigo_Cadastro.setColumns(10);
		
		txf_Teacher_Cadastro = new JTextField();
		txf_Teacher_Cadastro.setBounds(80, 68, 311, 22);
		panel.add(txf_Teacher_Cadastro);
		txf_Teacher_Cadastro.setColumns(10);
		
		JButton btn_Cadastro = new JButton("Cadastro");
		btn_Cadastro.setBackground(new Color(245,255,250));
		btn_Cadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String name = txf_Nome_Cadastro.getText();
				String cod = txf_Codigo_Cadastro.getText();
				String teacher = txf_Teacher_Cadastro.getText();
				
				DisciplineCTRL ctrl = new DisciplineCTRL();
				Discipline Discipline = new Discipline(cod,name,teacher);
				
				ctrl.Create(Discipline);	
			}
		});
		btn_Cadastro.setBounds(294, 103, 97, 25);
		panel.add(btn_Cadastro);
		
		JLabel label_7 = new JLabel("Nome:");
		label_7.setBounds(12, 10, 56, 16);
		panel.add(label_7);
		
		JLabel label_8 = new JLabel("C\u00F3digo:");
		label_8.setBounds(12, 39, 56, 16);
		panel.add(label_8);
		
		JLabel label_9 = new JLabel("Teacher:");
		label_9.setBounds(12, 68, 67, 16);
		panel.add(label_9);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Editar", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel label = new JLabel("Nome:");
		label.setBounds(12, 43, 56, 16);
		panel_1.add(label);
		
		txf_Nome_Editar = new JTextField();
		txf_Nome_Editar.setColumns(10);
		txf_Nome_Editar.setBounds(80, 40, 311, 22);
		panel_1.add(txf_Nome_Editar);
		
		JLabel lblCdigo_1 = new JLabel("C\u00F3digo:");
		lblCdigo_1.setBounds(12, 72, 56, 16);
		panel_1.add(lblCdigo_1);
		
		txf_Codigo_Editar = new JTextField();
		txf_Codigo_Editar.setColumns(10);
		txf_Codigo_Editar.setBounds(80, 69, 311, 22);
		panel_1.add(txf_Codigo_Editar);
		
		JLabel lblTeacher = new JLabel("Teacher:");
		lblTeacher.setBounds(12, 101, 67, 16);
		panel_1.add(lblTeacher);
		
		txf_Teacher_Editar = new JTextField();
		txf_Teacher_Editar.setColumns(10);
		txf_Teacher_Editar.setBounds(80, 98, 311, 22);
		panel_1.add(txf_Teacher_Editar);
		
		JButton btn_Editar = new JButton("Editar");
		btn_Editar.setBackground(new Color(245,255,250));
		btn_Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String name = txf_Nome_Editar.getText();
				String cod = txf_Codigo_Editar.getText();
				String teacher = txf_Teacher_Editar.getText();
				String key = txf_Key_Editar.getText();
				
				Discipline Discipline = new Discipline(cod,name,teacher);
				
				DisciplineCTRL ctrl = new DisciplineCTRL();
				ctrl.Update(Discipline, key);
				
			}
		});
		btn_Editar.setBounds(294, 133, 97, 25);
		panel_1.add(btn_Editar);
		
		JLabel lblRa_1 = new JLabel("C\u00F3digo:");
		lblRa_1.setBounds(12, 13, 56, 16);
		panel_1.add(lblRa_1);
		
		txf_Key_Editar = new JTextField();
		txf_Key_Editar.setBounds(80, 10, 116, 22);
		panel_1.add(txf_Key_Editar);
		txf_Key_Editar.setColumns(10);
		
		JButton btn_Pesquisar_Editar = new JButton("Pesquisar");
		btn_Pesquisar_Editar.setBackground(new Color(245,255,250));
		btn_Pesquisar_Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DisciplineDAO dao = new DisciplineDAO();
				
				DisciplineCTRL ctrl = new DisciplineCTRL();
				
				String key = txf_Key_Editar.getText();
				
				Discipline Discipline = ctrl.Read(key);
				
				txf_Nome_Editar.setText(Discipline.getName());
				txf_Codigo_Editar.setText(String.valueOf(Discipline.getCod()));
				txf_Teacher_Editar.setText(Discipline.getTeacher());
				
			}
		});
		btn_Pesquisar_Editar.setBounds(208, 9, 97, 25);
		panel_1.add(btn_Pesquisar_Editar);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Deletar", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblRa_2 = new JLabel("C\u00F3digo:");
		lblRa_2.setBounds(12, 13, 56, 16);
		panel_2.add(lblRa_2);
		
		txf_key_Deletar = new JTextField();
		txf_key_Deletar.setBounds(80, 10, 116, 22);
		panel_2.add(txf_key_Deletar);
		txf_key_Deletar.setColumns(10);
		
		JButton btn_Deletar = new JButton("Deletar");
		btn_Deletar.setBackground(new Color(245,255,250));
		btn_Deletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
                String key = txf_key_Deletar.getText();
				
                DisciplineCTRL ctrl = new DisciplineCTRL();
				
				ctrl.Delete(key);
				
			}
		});
		btn_Deletar.setBounds(208, 9, 97, 25);
		panel_2.add(btn_Deletar);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Pesquisa", null, panel_3, null);
		panel_3.setLayout(null);
		
		txf_Nome_Pesquisa = new JTextField();
		txf_Nome_Pesquisa.setBackground(new Color(245,245,245));
		txf_Nome_Pesquisa.setEditable(false);
		txf_Nome_Pesquisa.setColumns(10);
		txf_Nome_Pesquisa.setBounds(80, 58, 311, 22);
		panel_3.add(txf_Nome_Pesquisa);
		
		txf_Codigo_Pesquisa = new JTextField();
		txf_Codigo_Pesquisa.setBackground(new Color(245,245,245));
		txf_Codigo_Pesquisa.setEditable(false);
		txf_Codigo_Pesquisa.setColumns(10);
		txf_Codigo_Pesquisa.setBounds(80, 87, 311, 22);
		panel_3.add(txf_Codigo_Pesquisa);
		
		txf_Teacher_Pesquisa = new JTextField();
		txf_Teacher_Pesquisa.setBackground(new Color(245,245,245));
		txf_Teacher_Pesquisa.setEditable(false);
		txf_Teacher_Pesquisa.setColumns(10);
		txf_Teacher_Pesquisa.setBounds(80, 116, 311, 22);
		panel_3.add(txf_Teacher_Pesquisa);
		
		JLabel lblCdigo = new JLabel("C\u00F3digo:");
		lblCdigo.setBounds(12, 31, 56, 16);
		panel_3.add(lblCdigo);
		
		txf_Key_Pesquisa = new JTextField();
		txf_Key_Pesquisa.setColumns(10);
		txf_Key_Pesquisa.setBounds(80, 28, 116, 22);
		panel_3.add(txf_Key_Pesquisa);
		
		JButton btn_Pesquisar_Pesquisa = new JButton("Pesquisar");
		btn_Pesquisar_Pesquisa.setBackground(new Color(245,255,250));
		btn_Pesquisar_Pesquisa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DisciplineDAO dao = new DisciplineDAO();
				
				DisciplineCTRL ctrl = new DisciplineCTRL();
				
				String key = txf_Key_Editar.getText();
				
				Discipline Discipline = ctrl.Read(key);
				
				txf_Nome_Editar.setText(Discipline.getName());
				txf_Codigo_Editar.setText(Discipline.getCod());
				txf_Teacher_Editar.setText(Discipline.getTeacher());
				
			}
		});
		btn_Pesquisar_Pesquisa.setBounds(208, 27, 97, 25);
		panel_3.add(btn_Pesquisar_Pesquisa);
		
		JLabel label_3 = new JLabel("Nome:");
		label_3.setBounds(12, 58, 56, 16);
		panel_3.add(label_3);
		
		JLabel label_4 = new JLabel("C\u00F3digo:");
		label_4.setBounds(12, 87, 56, 16);
		panel_3.add(label_4);
		
		JLabel label_5 = new JLabel("Teacher:");
		label_5.setBounds(12, 116, 67, 16);
		panel_3.add(label_5);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Listagem", null, panel_4, null);
		panel_4.setLayout(null);
		
		JButton btn_Listar = new JButton("Listar");
		btn_Listar.setBackground(new Color(245,255,250));
		btn_Listar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DefaultTableModel modelTable = (DefaultTableModel) table.getModel();
				
				ArrayList<String> row= new ArrayList<String>();
				
				modelTable.setNumRows(0);
				
				DisciplineDAO dao = new DisciplineDAO();
				
				ArrayList<Discipline> listDiscipline = dao.List();
				
				for (Discipline a : listDiscipline ){
					
					row.add(a.getName());
					row.add(String.valueOf(a.getCod()));
					row.add(a.getTeacher());
					modelTable.addRow(row.toArray());
					row.clear();
				}
				
			}
		});
		btn_Listar.setBounds(294, 13, 97, 25);
		panel_4.add(btn_Listar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 55, 391, 103);
		panel_4.add(scrollPane);
		
		table = new JTable();
		table.setBackground(new Color(198,226,255));
		table.setFillsViewportHeight(true);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nome", "C\u00F3digo", "Teacher"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(DisciplineGUI.class.getResource("/view/img/Icone_disciplina3.png")));
		lblNewLabel.setBounds(315, 0, 183, 90);
		contentPane.add(lblNewLabel);
	}
}
