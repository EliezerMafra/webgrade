package view;
//classGUI
//de novo
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Class;
import model.Class;
import controler.ClassCTRL;
import controler.ClassCTRL;
import dao.ClassDAO;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import javax.swing.ImageIcon;

public class ClassGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txf_Cod_Cadastro;
	private JTextField txf_Modalidade_Cadastro;
	private JTextField txf_Ano_Cadastro;
	private JTextField txf_Modalidade_Editar;
	private JTextField txf_Cod_Editar;
	private JTextField txf_Curso_Editar;
	private JTextField txf_Key_Editar;
	private JTextField txf_Cod_Deletar;
	private JTextField txf_Modalidade_Pesquisa;
	private JTextField txf_Cod_Pesquisa;
	private JTextField txf_Curso_Pesquisa;
	private JTextField txf_Key_Pesquisa;
	private JTable table;
	private JTextField txf_Curso_Cadastro;
	private JTextField txf_Ano_Editar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClassGUI frame = new ClassGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClassGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 446, 326);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240,255,240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAluno = new JLabel("Turma");
		lblAluno.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblAluno.setBounds(163, 11, 143, 37);
		contentPane.add(lblAluno);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 48, 408, 228);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Cadastro", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Cod:");
		lblNome.setBounds(12, 13, 56, 16);
		panel.add(lblNome);
		
		txf_Cod_Cadastro = new JTextField();
		txf_Cod_Cadastro.setBounds(80, 10, 311, 22);
		panel.add(txf_Cod_Cadastro);
		txf_Cod_Cadastro.setColumns(10);
		
		JLabel lblRa = new JLabel("Modalidade:");
		lblRa.setBounds(12, 42, 56, 16);
		panel.add(lblRa);
		
		txf_Modalidade_Cadastro = new JTextField();
		txf_Modalidade_Cadastro.setBounds(80, 39, 311, 22);
		panel.add(txf_Modalidade_Cadastro);
		txf_Modalidade_Cadastro.setColumns(10);
		
		JLabel lblSrie = new JLabel("Ano:");
		lblSrie.setBounds(12, 71, 56, 16);
		panel.add(lblSrie);
		
		txf_Ano_Cadastro = new JTextField();
		txf_Ano_Cadastro.setBounds(80, 68, 311, 22);
		panel.add(txf_Ano_Cadastro);
		txf_Ano_Cadastro.setColumns(10);
		
		JButton btn_Cadastro = new JButton("Cadastro");
		btn_Cadastro.setBackground(new Color(245,255,250));
		btn_Cadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String cod = txf_Cod_Cadastro.getText();
				String modalidade = txf_Modalidade_Cadastro.getText();
				String ano = txf_Ano_Cadastro.getText();
				String curso= txf_Curso_Cadastro.getText();
				
				ClassCTRL ctrl = new ClassCTRL();
				Class Cclass = new Class(cod,modalidade,ano,curso);
				
				ctrl.Create(Cclass);
			}
		});
		btn_Cadastro.setBounds(294, 137, 97, 25);
		panel.add(btn_Cadastro);
		
		txf_Curso_Cadastro = new JTextField();
		txf_Curso_Cadastro.setBounds(80, 99, 311, 22);
		panel.add(txf_Curso_Cadastro);
		txf_Curso_Cadastro.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Curso:");
		lblNewLabel.setBounds(10, 103, 46, 14);
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Editar", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblModalidade = new JLabel("Modalidade:");
		lblModalidade.setBounds(12, 43, 56, 16);
		panel_1.add(lblModalidade);
		
		txf_Modalidade_Editar = new JTextField();
		txf_Modalidade_Editar.setColumns(10);
		txf_Modalidade_Editar.setBounds(80, 40, 311, 22);
		panel_1.add(txf_Modalidade_Editar);
		
		JLabel lblAno = new JLabel("Cod:");
		lblAno.setBounds(12, 72, 56, 16);
		panel_1.add(lblAno);
		
		txf_Cod_Editar = new JTextField();
		txf_Cod_Editar.setColumns(10);
		txf_Cod_Editar.setBounds(80, 69, 311, 22);
		panel_1.add(txf_Cod_Editar);
		
		JLabel lblDisciplina = new JLabel("Curso:");
		lblDisciplina.setBounds(12, 139, 56, 16);
		panel_1.add(lblDisciplina);
		
		txf_Curso_Editar = new JTextField();
		txf_Curso_Editar.setColumns(10);
		txf_Curso_Editar.setBounds(80, 136, 311, 22);
		panel_1.add(txf_Curso_Editar);
		
		JButton btn_Editar = new JButton("Editar");
		btn_Editar.setBackground(new Color(245,255,250));
		btn_Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String cod = txf_Cod_Editar.getText();
				String modalidade = txf_Modalidade_Editar.getText();
				String curso = txf_Curso_Editar.getText();
				int key = Integer.valueOf(txf_Key_Editar.getText());
				String ano = txf_Ano_Editar.getText();
				
				Class Class = new Class(cod,modalidade,ano,curso);
				
				ClassCTRL ctrl = new ClassCTRL();
				ctrl.Update(Class, key);
				
			}
		});
		btn_Editar.setBounds(296, 164, 97, 25);
		panel_1.add(btn_Editar);
		
		JLabel lblRa_1 = new JLabel("Cod:");
		lblRa_1.setBounds(12, 13, 56, 16);
		panel_1.add(lblRa_1);
		
		txf_Key_Editar = new JTextField();
		txf_Key_Editar.setBounds(80, 10, 116, 22);
		panel_1.add(txf_Key_Editar);
		txf_Key_Editar.setColumns(10);
		
		JButton btn_Pesquisar_Editar = new JButton("Pesquisar");
		btn_Pesquisar_Editar.setBackground(new Color(245,255,250));
		btn_Pesquisar_Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ClassDAO dao = new ClassDAO();
				
				ClassCTRL ctrl = new ClassCTRL();
				
				int key = Integer.valueOf(txf_Key_Editar.getText());
				
				Class Class = ctrl.Read(key);
				
				txf_Key_Editar.setText(Class.getCod());
				txf_Curso_Editar.setText(Class.getModalidade());
				txf_Curso_Editar.setText(Class.getAno());
				txf_Key_Editar.setText(Class.getCurso());
				
			}
		});
		btn_Pesquisar_Editar.setBounds(208, 9, 97, 25);
		panel_1.add(btn_Pesquisar_Editar);
		
		JLabel lblAno_1 = new JLabel("Ano:");
		lblAno_1.setBounds(12, 114, 46, 14);
		panel_1.add(lblAno_1);
		
		txf_Ano_Editar = new JTextField();
		txf_Ano_Editar.setBounds(78, 105, 313, 20);
		panel_1.add(txf_Ano_Editar);
		txf_Ano_Editar.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Deletar", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblRa_2 = new JLabel("Cod:");
		lblRa_2.setBounds(12, 13, 56, 16);
		panel_2.add(lblRa_2);
		
		txf_Cod_Deletar = new JTextField();
		txf_Cod_Deletar.setBounds(80, 10, 116, 22);
		panel_2.add(txf_Cod_Deletar);
		txf_Cod_Deletar.setColumns(10);
		
		JButton btn_Deletar = new JButton("Deletar");
		btn_Deletar.setBackground(new Color(245,255,250));
		btn_Deletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
                int ra = Integer.parseInt(txf_Cod_Deletar.getText());
				
                ClassCTRL ctrl = new ClassCTRL();
				
				ctrl.Delete(ra);
				
			}
		});
		btn_Deletar.setBounds(208, 9, 97, 25);
		panel_2.add(btn_Deletar);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Pesquisa", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel lblModalidade_1 = new JLabel("Modalidade:");
		lblModalidade_1.setBounds(12, 61, 56, 16);
		panel_3.add(lblModalidade_1);
		
		txf_Modalidade_Pesquisa = new JTextField();
		txf_Modalidade_Pesquisa.setEditable(false);
		txf_Modalidade_Pesquisa.setColumns(10);
		txf_Modalidade_Pesquisa.setBounds(80, 58, 311, 22);
		panel_3.add(txf_Modalidade_Pesquisa);
		
		JLabel lblCod_1 = new JLabel("Cod:");
		lblCod_1.setBounds(12, 90, 56, 16);
		panel_3.add(lblCod_1);
		
		txf_Cod_Pesquisa = new JTextField();
		txf_Cod_Pesquisa.setEditable(false);
		txf_Cod_Pesquisa.setColumns(10);
		txf_Cod_Pesquisa.setBounds(80, 87, 311, 22);
		panel_3.add(txf_Cod_Pesquisa);
		
		JLabel lblDisciplina_1 = new JLabel("Curso:");
		lblDisciplina_1.setBounds(12, 119, 56, 16);
		panel_3.add(lblDisciplina_1);
		
		txf_Curso_Pesquisa = new JTextField();
		txf_Curso_Pesquisa.setEditable(false);
		txf_Curso_Pesquisa.setColumns(10);
		txf_Curso_Pesquisa.setBounds(80, 116, 311, 22);
		panel_3.add(txf_Curso_Pesquisa);
		
		JLabel lblCod = new JLabel("Cod:");
		lblCod.setBounds(12, 31, 56, 16);
		panel_3.add(lblCod);
		
		txf_Key_Pesquisa = new JTextField();
		txf_Key_Pesquisa.setColumns(10);
		txf_Key_Pesquisa.setBounds(80, 28, 116, 22);
		panel_3.add(txf_Key_Pesquisa);
		
		JButton btn_Pesquisar_Pesquisa = new JButton("Pesquisar");
		btn_Pesquisar_Pesquisa.setBackground(new Color(245,255,250));
		btn_Pesquisar_Pesquisa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ClassDAO dao = new ClassDAO();
				
				ClassCTRL ctrl = new ClassCTRL();
				
				int key = Integer.valueOf(txf_Key_Editar.getText());
				
				Class Class = ctrl.Read(key);
				
				txf_Cod_Editar.setText(Class.getCod());
				txf_Modalidade_Editar.setText(String.valueOf(Class.getModalidade()));
				txf_Curso_Editar.setText(Class.getCurso());
				
			}
		});
		btn_Pesquisar_Pesquisa.setBounds(208, 27, 97, 25);
		panel_3.add(btn_Pesquisar_Pesquisa);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Listagem", null, panel_4, null);
		panel_4.setLayout(null);
		
		JButton btn_Listar = new JButton("Listar");
		btn_Listar.setBackground(new Color(245,255,250));
		btn_Listar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DefaultTableModel modelTable = (DefaultTableModel) table.getModel();
				
				ArrayList<String> row= new ArrayList<String>();
				
				modelTable.setNumRows(0);
				
				ClassDAO dao = new ClassDAO();
				
				ArrayList<Class> listClass = dao.List();
				
				for (Class a : listClass ){
					
				
					row.add(a.getCod());
					row.add(a.getModalidade());
					row.add(a.getAno());
					row.add(a.getCurso());
					modelTable.addRow(row.toArray());
					row.clear();
				}
				
			}
		});
		btn_Listar.setBounds(294, 13, 97, 25);
		panel_4.add(btn_Listar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 55, 383, 107);
		panel_4.add(scrollPane);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		table.setBackground(new Color(198,226,255));
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"C\u00F3digo", "Modalidade", "Ano", "Curso"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(ClassGUI.class.getResource("/view/img/humanos_icone2.png")));
		lblNewLabel_1.setBounds(213, 0, 217, 167);
		contentPane.add(lblNewLabel_1);
	}
}
