package view;
//TestGUI
//de novo
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Test;
import model.TestImport;
import model.Test;
import model.Test;
import controler.TestCTRL;
import controler.TestCTRL;
import dao.TestDAO;



import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import com.toedter.calendar.JDateChooser;

import java.awt.Color;

import javax.swing.ImageIcon;
//linha inutil para teste
//outra linha
//akdhgafkdghf
//xcxcxcxcvxc
public class TestGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txf_Cod_Cadastro_Test;
	private JTextField txf_Conteudo_Cadastro__Test;
	private JTextField txf_Conteudo_Editar_Test;
	private JTextField txf_Cod_Editar_Test;
	private JTextField txf_Key_Editar_Test;
	private JTextField txf_Cod_Deletar_Test;
	private JTextField txf_Conteudo_Pesquisa_Test;
	private JTextField txf_Cod_Pesquisa_Test;
	private JTextField txf_Data_Pesquisa_Test;
	private JTextField txf_Key_Pesquisa;
	private JTable table;
	private JTextField txf_Data_Editar_Test;
	private JTextField txf_nota_Cadastro;
	private JTextField txf_RaProfessor_Cadastro;
	private JTextField txf_codDisciplina_Cadastro;
	private JTextField txf_nota_Editar;
	private JTextField txf_RaProfessor_Editar;
	private JTextField txf_codDisciplina_Editar;
	private JTextField txf_nota_Pesquisa;
	private JTextField txf_RaProfessor_Pesquisa;
	private JTextField txf_codDisciplina_Pesquisa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestGUI frame = new TestGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 483, 406);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240,255,240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAluno = new JLabel("Prova");
		lblAluno.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblAluno.setBounds(163, 11, 143, 37);
		contentPane.add(lblAluno);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 48, 443, 298);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Cadastro", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Cod:");
		lblNome.setBounds(12, 13, 56, 16);
		panel.add(lblNome);
		
		txf_Cod_Cadastro_Test = new JTextField();
		txf_Cod_Cadastro_Test.setBounds(80, 10, 311, 22);
		panel.add(txf_Cod_Cadastro_Test);
		txf_Cod_Cadastro_Test.setColumns(10);
		
		JLabel lblRa = new JLabel("Conte\u00FAdo:");
		lblRa.setBounds(12, 42, 56, 16);
		panel.add(lblRa);
		
		txf_Conteudo_Cadastro__Test = new JTextField();
		txf_Conteudo_Cadastro__Test.setBounds(80, 39, 311, 54);
		panel.add(txf_Conteudo_Cadastro__Test);
		txf_Conteudo_Cadastro__Test.setColumns(10);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(80, 106, 311, 22);
		panel.add(dateChooser);
		
		JLabel lblSrie = new JLabel("Data:");
		lblSrie.setBounds(12, 107, 56, 16);
		panel.add(lblSrie);
		
		JButton btn_Cadastro = new JButton("Cadastro");
		btn_Cadastro.setBackground(new Color(245,255,250));
		btn_Cadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int cod = Integer.valueOf(txf_Cod_Cadastro_Test.getText());
				String conteudo = txf_Conteudo_Cadastro__Test.getText();
					
					//DATE
					java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
					String formatData = sdf.format(dateChooser.getDate());
				
				String data = formatData;
				int raTeacher = Integer.valueOf(txf_RaProfessor_Cadastro.getText());
				System.out.println("VAI ESTAR AQUIIII::::::::::"+raTeacher);
				int codDiscipline = Integer.valueOf(txf_codDisciplina_Cadastro.getText());
				double grade = Double.valueOf(txf_nota_Cadastro.getText());
			
				
				TestCTRL ctrl = new TestCTRL();
				Test Test = new Test(cod,conteudo,data);
				
				ctrl.Create(Test,raTeacher,codDiscipline,grade);
			}
		});
		btn_Cadastro.setBounds(294, 230, 97, 25);
		panel.add(btn_Cadastro);
		
		
		
		JLabel lblNota = new JLabel("Nota:");
		lblNota.setBounds(12, 136, 56, 16);
		panel.add(lblNota);
		
		JLabel lblRaProfessor = new JLabel("RA Professor:");
		lblRaProfessor.setBounds(12, 165, 56, 16);
		panel.add(lblRaProfessor);
		
		JLabel lblCodDisciplina = new JLabel("Cod Disciplina:");
		lblCodDisciplina.setBounds(12, 194, 56, 16);
		panel.add(lblCodDisciplina);
		
		txf_nota_Cadastro = new JTextField();
		txf_nota_Cadastro.setBounds(80, 133, 116, 22);
		panel.add(txf_nota_Cadastro);
		txf_nota_Cadastro.setColumns(10);
		
		txf_RaProfessor_Cadastro = new JTextField();
		txf_RaProfessor_Cadastro.setBounds(80, 162, 311, 22);
		panel.add(txf_RaProfessor_Cadastro);
		txf_RaProfessor_Cadastro.setColumns(10);
		
		txf_codDisciplina_Cadastro = new JTextField();
		txf_codDisciplina_Cadastro.setBounds(80, 191, 311, 22);
		panel.add(txf_codDisciplina_Cadastro);
		txf_codDisciplina_Cadastro.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Editar", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblConteudo = new JLabel("Conte\u00FAdo:");
		lblConteudo.setBounds(12, 43, 56, 16);
		panel_1.add(lblConteudo);
		
		txf_Conteudo_Editar_Test = new JTextField();
		txf_Conteudo_Editar_Test.setColumns(10);
		txf_Conteudo_Editar_Test.setBounds(80, 40, 311, 22);
		panel_1.add(txf_Conteudo_Editar_Test);
		
		JLabel lblAno = new JLabel("Cod:");
		lblAno.setBounds(12, 72, 56, 16);
		panel_1.add(lblAno);
		
		txf_Cod_Editar_Test = new JTextField();
		txf_Cod_Editar_Test.setColumns(10);
		txf_Cod_Editar_Test.setBounds(80, 69, 311, 22);
		panel_1.add(txf_Cod_Editar_Test);
		
		JButton btn_Editar = new JButton("Editar");
		btn_Editar.setBackground(new Color(245,255,250));
		btn_Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
		
				int key = Integer.valueOf(txf_Key_Editar_Test.getText());
				int cod = Integer.valueOf(txf_Cod_Editar_Test.getText());
				String conteudo = txf_Conteudo_Editar_Test.getText();
					
					//DATE
					java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
					String formatData = sdf.format(dateChooser.getDate());
				
				String data = formatData;
				int raTeacher = Integer.valueOf(txf_RaProfessor_Editar.getText());
				int codDiscipline = Integer.valueOf(txf_codDisciplina_Editar.getText());
				double grade = Double.valueOf(txf_nota_Editar.getText());
			
				
				TestCTRL ctrl = new TestCTRL();
				Test Test = new Test(cod,conteudo,data);
				
				ctrl.Update(Test,raTeacher,codDiscipline,grade,key);
				
			}
		});
		btn_Editar.setBounds(294, 230, 97, 25);
		panel_1.add(btn_Editar);
		
		JLabel lblRa_1 = new JLabel("Cod:");
		lblRa_1.setBounds(12, 13, 56, 16);
		panel_1.add(lblRa_1);
		
		txf_Key_Editar_Test = new JTextField();
		txf_Key_Editar_Test.setBounds(80, 10, 116, 22);
		panel_1.add(txf_Key_Editar_Test);
		txf_Key_Editar_Test.setColumns(10);
		
		JButton btn_Pesquisar_Editar = new JButton("Pesquisar");
		btn_Pesquisar_Editar.setBackground(new Color(245,255,250));
		btn_Pesquisar_Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				TestDAO dao = new TestDAO();
				
				TestCTRL ctrl = new TestCTRL();
				
				int key = Integer.valueOf(txf_Key_Editar_Test.getText());
				
				TestImport Test = ctrl.Read(key);
				Test normalTest = Test.getTest();
				
				
				txf_Cod_Editar_Test.setText(String.valueOf(normalTest.getCod()));
				txf_Conteudo_Editar_Test.setText(normalTest.getContents());
				txf_Data_Editar_Test.setText(normalTest.getData());
				txf_nota_Editar.setText(String.valueOf(Test.getGrade()));
				txf_RaProfessor_Editar.setText(String.valueOf(Test.getCodDisciplina()));
				
				
				
			}
		});
		btn_Pesquisar_Editar.setBounds(208, 9, 97, 25);
		panel_1.add(btn_Pesquisar_Editar);
		
		JLabel lblAno_1 = new JLabel("Data:");
		lblAno_1.setBounds(12, 114, 46, 14);
		panel_1.add(lblAno_1);
		
		txf_Data_Editar_Test = new JTextField();
		txf_Data_Editar_Test.setBounds(78, 105, 313, 20);
		panel_1.add(txf_Data_Editar_Test);
		txf_Data_Editar_Test.setColumns(10);
		
		JLabel label = new JLabel("Nota:");
		label.setBounds(12, 140, 56, 16);
		panel_1.add(label);
		
		txf_nota_Editar = new JTextField();
		txf_nota_Editar.setColumns(10);
		txf_nota_Editar.setBounds(80, 137, 116, 22);
		panel_1.add(txf_nota_Editar);
		
		JLabel label_1 = new JLabel("RA Professor:");
		label_1.setBounds(12, 169, 56, 16);
		panel_1.add(label_1);
		
		txf_RaProfessor_Editar = new JTextField();
		txf_RaProfessor_Editar.setColumns(10);
		txf_RaProfessor_Editar.setBounds(80, 166, 311, 22);
		panel_1.add(txf_RaProfessor_Editar);
		
		JLabel label_2 = new JLabel("Cod Disciplina:");
		label_2.setBounds(12, 198, 56, 16);
		panel_1.add(label_2);
		
		txf_codDisciplina_Editar = new JTextField();
		txf_codDisciplina_Editar.setColumns(10);
		txf_codDisciplina_Editar.setBounds(80, 195, 311, 22);
		panel_1.add(txf_codDisciplina_Editar);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Deletar", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblRa_2 = new JLabel("Cod:");
		lblRa_2.setBounds(12, 13, 56, 16);
		panel_2.add(lblRa_2);
		
		txf_Cod_Deletar_Test = new JTextField();
		txf_Cod_Deletar_Test.setBounds(80, 10, 116, 22);
		panel_2.add(txf_Cod_Deletar_Test);
		txf_Cod_Deletar_Test.setColumns(10);
		
		JButton btn_Deletar = new JButton("Deletar");
		btn_Deletar.setBackground(new Color(245,255,250));
		btn_Deletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
                int ra = Integer.parseInt(txf_Cod_Deletar_Test.getText());
				
                TestCTRL ctrl = new TestCTRL();
				
				ctrl.Delete(ra);
				
			}
		});
		btn_Deletar.setBounds(208, 9, 97, 25);
		panel_2.add(btn_Deletar);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Pesquisa", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel lblconteudo_1 = new JLabel("Conte\u00FAdo:");
		lblconteudo_1.setBounds(12, 61, 56, 16);
		panel_3.add(lblconteudo_1);
		
		txf_Conteudo_Pesquisa_Test = new JTextField();
		txf_Conteudo_Pesquisa_Test.setEditable(false);
		txf_Conteudo_Pesquisa_Test.setColumns(10);
		txf_Conteudo_Pesquisa_Test.setBounds(80, 58, 311, 22);
		panel_3.add(txf_Conteudo_Pesquisa_Test);
		
		JLabel lblCod_1 = new JLabel("Cod:");
		lblCod_1.setBounds(12, 90, 56, 16);
		panel_3.add(lblCod_1);
		
		txf_Cod_Pesquisa_Test = new JTextField();
		txf_Cod_Pesquisa_Test.setEditable(false);
		txf_Cod_Pesquisa_Test.setColumns(10);
		txf_Cod_Pesquisa_Test.setBounds(80, 87, 311, 22);
		panel_3.add(txf_Cod_Pesquisa_Test);
		
		JLabel lblDisciplina_1 = new JLabel("Data:");
		lblDisciplina_1.setBounds(12, 119, 56, 16);
		panel_3.add(lblDisciplina_1);
		
		txf_Data_Pesquisa_Test = new JTextField();
		txf_Data_Pesquisa_Test.setEditable(false);
		txf_Data_Pesquisa_Test.setColumns(10);
		txf_Data_Pesquisa_Test.setBounds(80, 116, 311, 22);
		panel_3.add(txf_Data_Pesquisa_Test);
		
		JLabel lblCod = new JLabel("Cod:");
		lblCod.setBounds(12, 31, 56, 16);
		panel_3.add(lblCod);
		
		txf_Key_Pesquisa = new JTextField();
		txf_Key_Pesquisa.setColumns(10);
		txf_Key_Pesquisa.setBounds(80, 28, 116, 22);
		panel_3.add(txf_Key_Pesquisa);
		
		JButton btn_Pesquisar_Pesquisa = new JButton("Pesquisar");
		btn_Pesquisar_Pesquisa.setBackground(new Color(245,255,250));
		btn_Pesquisar_Pesquisa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				TestDAO dao = new TestDAO();
				
				TestCTRL ctrl = new TestCTRL();
				
				int key = Integer.valueOf(txf_Key_Editar_Test.getText());
				
				TestImport Test = ctrl.Read(key);
				Test normalTest = Test.getTest();
				
				
				txf_Cod_Pesquisa_Test.setText(String.valueOf(normalTest.getCod()));
				txf_Conteudo_Pesquisa_Test.setText(normalTest.getContents());
				txf_Data_Pesquisa_Test.setText(normalTest.getData());
				txf_nota_Pesquisa.setText(String.valueOf(Test.getGrade()));
				txf_RaProfessor_Pesquisa.setText(String.valueOf(Test.getCodDisciplina()));
				
			}
		});
		btn_Pesquisar_Pesquisa.setBounds(208, 27, 97, 25);
		panel_3.add(btn_Pesquisar_Pesquisa);
		
		JLabel label_3 = new JLabel("Nota:");
		label_3.setBounds(12, 151, 56, 16);
		panel_3.add(label_3);
		
		txf_nota_Pesquisa = new JTextField();
		txf_nota_Pesquisa.setEditable(false);
		txf_nota_Pesquisa.setColumns(10);
		txf_nota_Pesquisa.setBounds(80, 148, 116, 22);
		panel_3.add(txf_nota_Pesquisa);
		
		JLabel label_4 = new JLabel("RA Professor:");
		label_4.setBounds(12, 180, 56, 16);
		panel_3.add(label_4);
		
		txf_RaProfessor_Pesquisa = new JTextField();
		txf_RaProfessor_Pesquisa.setEditable(false);
		txf_RaProfessor_Pesquisa.setColumns(10);
		txf_RaProfessor_Pesquisa.setBounds(80, 177, 311, 22);
		panel_3.add(txf_RaProfessor_Pesquisa);
		
		JLabel label_5 = new JLabel("Cod Disciplina:");
		label_5.setBounds(12, 209, 56, 16);
		panel_3.add(label_5);
		
		txf_codDisciplina_Pesquisa = new JTextField();
		txf_codDisciplina_Pesquisa.setEditable(false);
		txf_codDisciplina_Pesquisa.setColumns(10);
		txf_codDisciplina_Pesquisa.setBounds(80, 206, 311, 22);
		panel_3.add(txf_codDisciplina_Pesquisa);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(240,248,255));
		tabbedPane.addTab("Listagem", null, panel_4, null);
		panel_4.setLayout(null);
		
		JButton btn_Listar_Test = new JButton("Listar");
		btn_Listar_Test.setBackground(new Color(245,255,250));
		btn_Listar_Test.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DefaultTableModel modelTable = (DefaultTableModel) table.getModel();
				
				ArrayList<String> row= new ArrayList<String>();
				
				modelTable.setNumRows(0);
				
				TestDAO dao = new TestDAO();
				
				ArrayList<TestImport> listTest = dao.List();
				
				for (TestImport b : listTest ){
					Test a = new Test();
					a=b.getTest();
				
					row.add(String.valueOf(a.getCod()));
					row.add(a.getData());
					row.add(String.valueOf(b.getGrade()));
					row.add(String.valueOf(b.getRaTeacher()));
					row.add(String.valueOf(b.getCodDisciplina()));
					modelTable.addRow(row.toArray());
					row.clear();
				}
				
			}
		});
		btn_Listar_Test.setBounds(294, 13, 97, 25);
		panel_4.add(btn_Listar_Test);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 55, 383, 107);
		panel_4.add(scrollPane);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		table.setBackground(new Color(198,226,255));
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"C\u00F3digo", "Conte\u00FAdo", "Data", "Nota", "RA Professor", "C\u00F3digo disciplina"
			}
		));
		table.getColumnModel().getColumn(4).setPreferredWidth(104);
		table.getColumnModel().getColumn(5).setPreferredWidth(122);
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(TestGUI.class.getResource("/view/img/Icone_test.png")));
		lblNewLabel_1.setBounds(282, -19, 171, 109);
		contentPane.add(lblNewLabel_1);
	}
}
