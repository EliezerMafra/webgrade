package view.MainGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Student;
import view.ClassGUI;
import view.StudentGUI;
import view.DisciplineGUI;
import view.TeacherGUI;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;

public class AdminMainGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminMainGUI frame = new AdminMainGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminMainGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAdmin = new JLabel("Admin");
		lblAdmin.setForeground(Color.WHITE);
		lblAdmin.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblAdmin.setBounds(161, 11, 138, 37);
		contentPane.add(lblAdmin);
		
		JComboBox cbOp = new JComboBox();
		cbOp.setBackground(new Color(248,248,255));
		cbOp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String op = cbOp.getSelectedItem().toString();
				StudentGUI s = new StudentGUI();
				DisciplineGUI d = new DisciplineGUI();
				ClassGUI c = new ClassGUI();
				TeacherGUI te = new TeacherGUI();
				
				
				
				
				if(op.equals("Cadastrar Aluno"))
					s.setVisible(true);
				
				if(op.equals("Cadastrar Professor"))
					te.setVisible(true);
				
				if(op.equals("Cadastrar Disciplina"))
					d.setVisible(true);
				
				if(op.equals("Cadastrar Turma"))
					c.setVisible(true);
				
			
				
				}
				
				
				
				
				
				
				//dispose();
				
				
			
		});
		cbOp.setModel(new DefaultComboBoxModel(new String[] {"", "Cadastrar Professor", "Cadastrar Aluno", "Cadastrar Turma", "Cadastrar Disciplina"}));
		cbOp.setBounds(122, 112, 184, 22);
		contentPane.add(cbOp);
		
		JLabel lblSelecioneUmaAo = new JLabel("Selecione uma a\u00E7\u00E3o:");
		lblSelecioneUmaAo.setForeground(Color.WHITE);
		lblSelecioneUmaAo.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSelecioneUmaAo.setBounds(122, 79, 218, 22);
		contentPane.add(lblSelecioneUmaAo);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
				
			}
		});
		btnFechar.setBounds(12, 215, 97, 25);
		contentPane.add(btnFechar);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(AdminMainGUI.class.getResource("/view/img/Admin1.jpg")));
		label.setBounds(-32, -45, 499, 336);
		contentPane.add(label);
	}
}
