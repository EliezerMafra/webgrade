package view.MainGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import view.TestGUI;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;

public class StudentMainGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentMainGUI frame = new StudentMainGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentMainGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAluno = new JLabel("Aluno");
		lblAluno.setForeground(Color.WHITE);
		lblAluno.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblAluno.setBounds(172, 11, 152, 42);
		contentPane.add(lblAluno);
		
		JComboBox cbOp = new JComboBox();
		cbOp.setBackground(new Color(248,248,255));
		cbOp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String op = cbOp.getSelectedItem().toString();
				TestGUI t = new TestGUI();
				
				
					t.setVisible(true);
				
				
		//dispose();
				
				
			}
		});
		cbOp.setModel(new DefaultComboBoxModel(new String[] {"", "Visualizar Provas"}));
		cbOp.setBounds(119, 124, 184, 22);
		contentPane.add(cbOp);
		
		JLabel lblSelecioneUmaAo = new JLabel("Selecione uma a\u00E7\u00E3o:");
		lblSelecioneUmaAo.setForeground(Color.WHITE);
		lblSelecioneUmaAo.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSelecioneUmaAo.setBounds(127, 78, 218, 22);
		contentPane.add(lblSelecioneUmaAo);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.setBackground(new Color(198,226,255));
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
				
			}
		});
		btnFechar.setBounds(12, 215, 97, 25);
		contentPane.add(btnFechar);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(StudentMainGUI.class.getResource("/view/img/estudante1.jpg")));
		label.setBounds(-45, -115, 569, 522);
		contentPane.add(label);
	}
}
