package controler;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Student;
import dao.StudentDAO;

public class StudentCTRL {
public void Create(Student student){
		
		StudentDAO dao = new StudentDAO();
		
		if(dao.Create(student)){
			JOptionPane.showMessageDialog(null, "INSERIDO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public void Update(Student student, int key ){
		
		StudentDAO dao = new StudentDAO();
		
		if(dao.Update(student, key)){
			JOptionPane.showMessageDialog(null, "EDITADO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public void Delete(int key ){
		
		StudentDAO dao = new StudentDAO();
		
		if(dao.Delete(key)){
			JOptionPane.showMessageDialog(null, "DELETADO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public Student Read(int key){
		
		StudentDAO dao = new StudentDAO();
		
		return dao.Read(key);
		
	}
	
	public ArrayList<Student> List(){
		
		StudentDAO dao = new StudentDAO();
		
		return dao.List();
	}
}


