package controler;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Discipline;
import dao.DisciplineDAO;

public class DisciplineCTRL {
public void Create(Discipline Discipline){
		
		DisciplineDAO dao = new DisciplineDAO();
		
		if(dao.Create(Discipline)){
			JOptionPane.showMessageDialog(null, "INSERIDO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public void Update(Discipline Discipline, String key ){
		
		DisciplineDAO dao = new DisciplineDAO();
		
		if(dao.Update(Discipline, key)){
			JOptionPane.showMessageDialog(null, "EDITADO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public void Delete(String key ){
		
		DisciplineDAO dao = new DisciplineDAO();
		
		if(dao.Delete(key)){
			JOptionPane.showMessageDialog(null, "DELETADO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public Discipline Read(String key){
		
		DisciplineDAO dao = new DisciplineDAO();
		
		return dao.Read(key);
		
	}
	
	public ArrayList<Discipline> List(){
		
		DisciplineDAO dao = new DisciplineDAO();
		
		return dao.List();
	}
}