package controler;
//TESTE PC ISABELLA
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Test;
import model.TestImport;
import model.Discipline;
import dao.TestDAO;
import dao.DisciplineDAO;

public class TestCTRL {
		public void Create(Test Test, int raTeacher, int codDiscipline, double grade){
				
				TestDAO dao = new TestDAO();
				
				if(dao.Create(Test,raTeacher,codDiscipline,grade)){
					JOptionPane.showMessageDialog(null, "INSERIDO COM SUCESSO!!");
				}else{
					JOptionPane.showMessageDialog(null, "ERROR!!");
				}
				
			}
			
			public void Update(Test Test, int raTeacher, int codDisciplina, double grade, int key){
				
				TestDAO dao = new TestDAO();
				
				if(dao.Update(Test,raTeacher,codDisciplina,grade, key)){
					JOptionPane.showMessageDialog(null, "EDITADO COM SUCESSO!!");
				}else{
					JOptionPane.showMessageDialog(null, "ERROR!!");
				}
				
			}
			
			public void Delete(int key ){
				
				TestDAO dao = new TestDAO();
				
				if(dao.Delete(key)){
					JOptionPane.showMessageDialog(null, "DELETADO COM SUCESSO!!");
				}else{
					JOptionPane.showMessageDialog(null, "ERROR!!");
				}
				
			}
			
			public TestImport Read(int key){
				
				TestDAO dao = new TestDAO();
				
				return dao.Read(key);
				
			}
			
			public ArrayList<TestImport> List(){
				
				TestDAO dao = new TestDAO();
				
				return dao.List();
			}
		}
	