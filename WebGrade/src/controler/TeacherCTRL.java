package controler;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Teacher;
import dao.TeacherDAO;
//teacherCTRL

public class TeacherCTRL {
public void Create(Teacher teacher){
		
		TeacherDAO dao = new TeacherDAO();
		
		if(dao.Create(teacher)){
			JOptionPane.showMessageDialog(null, "INSERIDO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public void Update(Teacher teacher, int key ){
		
		TeacherDAO dao = new TeacherDAO();
		
		if(dao.Update(teacher, key)){
			JOptionPane.showMessageDialog(null, "EDITADO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public void Delete(int key ){
		
		TeacherDAO dao = new TeacherDAO();
		
		if(dao.Delete(key)){
			JOptionPane.showMessageDialog(null, "DELETADO COM SUCESSO!!");
		}else{
			JOptionPane.showMessageDialog(null, "ERROR!!");
		}
		
	}
	
	public Teacher Read(int key){
		
		TeacherDAO dao = new TeacherDAO();
		
		return dao.Read(key);
		
	}
	
	public ArrayList<Teacher> List(){
		
		TeacherDAO dao = new TeacherDAO();
		
		return dao.List();
	}
}


