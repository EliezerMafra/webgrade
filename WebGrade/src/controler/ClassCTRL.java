package controler;
//TESTE PC ISABELLA
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Class;
import model.Discipline;
import dao.ClassDAO;
import dao.DisciplineDAO;

public class ClassCTRL {
		public void Create(Class Cclass){
				
				ClassDAO dao = new ClassDAO();
				
				if(dao.Create(Cclass)){
					JOptionPane.showMessageDialog(null, "INSERIDO COM SUCESSO!!");
				}else{
					JOptionPane.showMessageDialog(null, "ERROR!!");
				}
				
			}
			
			public void Update(Class Cclass, int key ){
				
				ClassDAO dao = new ClassDAO();
				
				if(dao.Update(Cclass, key)){
					JOptionPane.showMessageDialog(null, "EDITADO COM SUCESSO!!");
				}else{
					JOptionPane.showMessageDialog(null, "ERROR!!");
				}
				
			}
			
			public void Delete(int key ){
				
				ClassDAO dao = new ClassDAO();
				
				if(dao.Delete(key)){
					JOptionPane.showMessageDialog(null, "DELETADO COM SUCESSO!!");
				}else{
					JOptionPane.showMessageDialog(null, "ERROR!!");
				}
				
			}
			
			public Class Read(int key){
				
				ClassDAO dao = new ClassDAO();
				
				return dao.Read(key);
				
			}
			
			public ArrayList<Class> List(){
				
				ClassDAO dao = new ClassDAO();
				
				return dao.List();
			}
		}
	