package model;

public class TestImport {

	private Test test = new Test();
	private double grade;
	private int raTeacher;
	private int codDisciplina;
	
	public TestImport() {
		super();
	}

	public TestImport(Test test, double grade, int raTeacher, int codDisciplina) {
		super();
		this.test = test;
		this.grade = grade;
		this.raTeacher = raTeacher;
		this.codDisciplina = codDisciplina;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double grade) {
		this.grade = grade;
	}

	public int getRaTeacher() {
		return raTeacher;
	}

	public void setRaTeacher(int raTeacher) {
		this.raTeacher = raTeacher;
	}

	public int getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(int codDisciplina) {
		this.codDisciplina = codDisciplina;
	}
	
	
	
	
	
}
