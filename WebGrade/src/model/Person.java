package model;

public class Person {

	private String name;
	private int ra;
	
	//CTR
	public Person() {
		super();
	}

	public Person(String name, int ra) {
		super();
		this.name = name;
		this.ra = ra;
	}

	//G&S
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRa() {
		return ra;
	}

	public void setRa(int ra) {
		this.ra = ra;
	}
	
	
}
