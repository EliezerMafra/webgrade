package model;

public class Class{
	
	private String cod;
	private String modalidade;
	private String ano;
	private String curso;
	

	public Class() {
		super();
	}


	public Class(String cod, String modalidade, String ano, String curso) {
		super();
		this.cod = cod;
		this.modalidade = modalidade;
		this.ano = ano;
		this.curso = curso;
	}


	public String getCod() {
		return cod;
	}


	public void setCod(String cod) {
		this.cod = cod;
	}


	public String getModalidade() {
		return modalidade;
	}


	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}


	public String getAno() {
		return ano;
	}


	public void setAno(String ano) {
		this.ano = ano;
	}


	public String getCurso() {
		return curso;
	}


	public void setCurso(String curso) {
		this.curso = curso;
	}


	

}
