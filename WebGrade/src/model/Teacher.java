package model;

public class Teacher extends Person{
	
	private String discipline;
	private Discipline disciplinaMinistrada;
	private String nomeD;
	
	

	public Teacher() {
		super();
		this.disciplinaMinistrada = new Discipline();
	}

	public Teacher(String name, int ra, String coddiscipline) {
		super(name, ra);
		//criar um objeto disciplinactrl 
		//disciplinactrl.getdisciplinabycode(coddiscipline)
		this.disciplinaMinistrada = new Discipline();
	}

	public String getDiscipline() {
		return discipline;
	}

	public void setDiscipline(String discipline) {
		this.discipline = discipline;
	}
	
	

}
