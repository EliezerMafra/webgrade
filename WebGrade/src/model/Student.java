package model;

public class Student extends Person{
	
	private String serie;

	public Student() {
		super();
	}

	public Student(String name, int ra, String serie) {
		super(name, ra);
		this.serie = serie;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}
	
	

}
