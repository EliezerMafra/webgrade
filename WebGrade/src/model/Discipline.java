package model;

public class Discipline {
	
	private String cod;
	private String name;
	private String teacher;

	
	//CTR
		public Discipline() {
			super();
		}


	
	public Discipline(String cod, String name, String teacher) {
			super();
			this.cod = cod;
			this.name = name;
			this.teacher = teacher;
		}



	//G&S
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	
	

}
