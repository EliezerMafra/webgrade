package model;

import java.text.SimpleDateFormat;

public class Test {
	
	private int cod;
	private String contents;
	private String data;
	
	//CONSTRUCTORS
	public Test() {
		super();
	}

	public Test(int cod, String contents, String data) {
		super();
		this.cod = cod;
		this.contents = contents;
		this.setData(data);
	}
	
	//G&S
	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}	

}
